function add_to_cart(id)
{
  let key = 'product_' + id; // ключ

  let x = window.localStorage.getItem(key);
  x = x * 1 + 1; // значение
  window.localStorage.setItem(key, x); //получаем хэш

  update_orders_input();
  update_orders_button();
}

function update_orders_button(){
  
  let text = 'Cart (' + cart_get_number_of_items() + ')';
  $('#orders_button').val(text);

}

function update_orders_input()
{
  let orders = cart_get_orders();
  $('#orders_input').val(orders);
}

function cart_get_number_of_items()
{
  let cnt = 0;

  for (let i = 0; i < window.localStorage.length; i++)
  {
    let key = window.localStorage.key(i); //получаем ключ
    let value = window.localStorage.getItem(key); //получаем значение

    if(key.indexOf('product_') == 0) 
    {
      cnt = cnt + value * 1;
    }
  }
  return cnt;
}

function cart_get_orders()
{
  let orders = '';

  for (let i = 0; i < window.localStorage.length; i++)
  {
    let key = window.localStorage.key(i); //получаем ключ
    let value = window.localStorage.getItem(key); //получаем значение

    if(key.indexOf('product_') == 0) 
    {
      orders = orders + key + '=' + value + ',';
    }
  }
  return orders;
}

function cancel_order() 
{
  window.localStorage.clear();

  update_orders_input();
  update_orders_button();
  
  $('#cart').text('Your cart is now empty')

  return false;
}